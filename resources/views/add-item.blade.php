@extends('layouts.app')

@section('title', 'Add item')

@section('content')

<form method="POST" action="{{ url('/add-item') }}" role="form" class="mdl-grid">
  {!! csrf_field() !!}

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <input class="mdl-textfield__input" type="text" name="name" id="name" required>
    <label class="mdl-textfield__label" for="name">Item name</label>
  </div>
  <div class="mdl-selectfield mdl-cell mdl-cell--12-col">
    <h6>Type</h6>
    <select class="browser-default" name="type">
      @foreach ($types as $type)
        <option value="{{$type->id}}">{{$type->name}}</option>
      @endforeach
    </select>
  </div>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <textarea class="mdl-textfield__input" type="text" rows= "2" name="description" id="description" required></textarea>
    <label class="mdl-textfield__label" for="description">Description</label>
  </div>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <textarea class="mdl-textfield__input" type="text" rows= "2" name="wishfor" id="wishfor" required></textarea>
    <label class="mdl-textfield__label" for="wishfor">Wish For (Split by comma)</label>
  </div>
  <label class="mdl-switch mdl-js-switch mdl-cell mdl-cell--12-col mdl-js-ripple-effect" for="giveaway">
    <input type="checkbox" id="giveaway" name="giveaway" class="mdl-switch__input">
    <span class="mdl-switch__label">Giveaway?</span>
  </label>
  <button type="submit" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
    <i class="material-icons">done</i>
  </button>
</form>
@endsection
