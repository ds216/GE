@extends('layouts.app')

@section('title', 'Donations')

@section('content')

<div class="mdl-grid">
  @foreach($donations as $donation)
  <a href="/donation/{{$donation->id}}" class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col">
    <div class="mdl-card__title mdl-card--expand">
      <h2 class="mdl-card__title-text">{{$donation->name}}</h2>
    </div>
    <div class="mdl-card__supporting-text">
      {{$donation->charity}}
    </div>
    <div class="mdl-card__supporting-text">
      {{$donation->description}}
    </div>
  </a>
  @endforeach()
</div>

<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" href="/add-item">
  <i class="material-icons">add</i>
</a>
@endsection
