@extends('layouts.login')

@section('title', "Welcome")
@section('content')
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--4-col">
    <img class="logo" src="{{url('/images/logo.png')}}" alt="logo" />
  </div>
</div>

<div class="mdl-grid">
  <a href="{{ url('/login') }}" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
    Login
  </a>
  <a href="{{ url('/register') }}" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-button--colored mdl-js-ripple-effect">
    Register
  </a>  
</div>

@endsection
