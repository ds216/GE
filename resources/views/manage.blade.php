@extends('layouts.app')

@section('title', "Management")

@section('content')
<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
<div class="mdl-tabs__tab-bar">
    <a href="#selling-panel" class="mdl-tabs__tab is-active">Selling</a>
    <a href="#offers-panel" class="mdl-tabs__tab">Offers</a>
    <a href="#donation-panel" class="mdl-tabs__tab">Donations</a>
</div>

<div class="mdl-tabs__panel is-active" id="selling-panel">
  @foreach($selling as $item)
  <a href="/item/{{$item->id}}" class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col">
    <div class="mdl-card__title mdl-card--expand">
      <h2 class="mdl-card__title-text">{{$item->name}}</h2>
    </div>
    <div class="mdl-card__supporting-text">
      Sold by {{$item->user->name}}
    </div>
  </a>
  @endforeach()
</div>
<div class="mdl-tabs__panel" id="offers-panel">
  @foreach($buying as $offer)
  <a href="/item/{{$offer->item->id}}" class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col">
    <div class="mdl-card__title mdl-card--expand">
      <h2 class="mdl-card__title-text">{{$offer->item->name}}</h2>
    </div>
    <div class="mdl-card__supporting-text">
      Sold by {{$offer->item->user->name}}
    </div>
  </a>
  @endforeach()
</div>
<div class="mdl-tabs__panel" id="donation-panel">
  @foreach($donations as $donation)
  <a href="/donation/{{$donation->donation->id}}" class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col">
    <div class="mdl-card__title mdl-card--expand">
      <h2 class="mdl-card__title-text">{{$donation->donation->name}}</h2>
    </div>
    <div class="mdl-card__supporting-text">
      <div class="">
        Urged by {{$donation->donation->charity}}
      </div>
      <div class="">
        Donated {{$donation->description}}
      </div>
      @if(is_null($donation->accepted))
      <div class="status_txt warning">
        Not yet decided
      </div>
      @elseif($donation->accepted == true)
      <div class="status_txt safe">
        Charity accepted and it will contact you immediately
      </div>
      @else
      <div class="status_txt danger">
        Charity didn't accept this donation
      </div>

      @endif()
    </div>
  </a>
  @endforeach()
</div>
</div>
@endsection
