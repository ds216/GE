@extends('layouts.app')

@section('title', 'Add Wishlist')

@section('content')

<form method="POST" action="{{ url('/add-wishlist') }}" role="form">
  {!! csrf_field() !!}

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <input class="mdl-textfield__input" type="text" name="name" id="name" required>
    <label class="mdl-textfield__label" for="name">Item name</label>
  </div>
  <button type="submit" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
    <i class="material-icons">done</i>
  </button>
</form>
@endsection
