@extends('layouts.app')

@section('title', $donation->name)

@section('content')
<div class="content">
  <div class="mdl-card fullscreen">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">{{$donation->name}}</h2>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Started by
      </div>
      <div class="mdl-card__supporting-text">
        {{$donation->charity}}
      </div>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Description
      </div>
      <div class="mdl-card__supporting-text">
        {{$donation->description}}
      </div>
    </div>
  </div>
  @if(count($donation_items) > 0)
  <table class="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col">
    <thead>
      <tr>
        <th class="mdl-data-table__cell--non-numeric">Name</th>
        <th class="mdl-data-table__cell--non-numeric">Type</th>
        <th class="mdl-data-table__cell--non-numeric">Quantity</th>
      </tr>
    </thead>
    <tbody>
      @foreach($donation_items as $item)
      <tr>
        <td class="mdl-data-table__cell--non-numeric">{{$item->item_name}}</td>
        <td class="mdl-data-table__cell--non-numeric">{{$item->type->name}}</td>
        <td class="mdl-data-table__cell--non-numeric">{{$item->quantity}}</td>
      </tr>
      @endforeach()
    </tbody>
  </table>
  <form method="POST" action="{{ url('/add-donation-request') }}" role="form">
    <h4 class="u-pl16">I want to donate...</h4>
    {!! csrf_field() !!}
    <input type="hidden" name="donator_id" value="{{Auth::user()->id}}">
    <input type="hidden" name="donation_id" value="{{$donation->id}}">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
      <input class="mdl-textfield__input" type="text" name="thing" id="name" required>
      <label class="mdl-textfield__label" for="thing">Thing I want to donate</label>
    </div>
    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-cell mdl-cell--12-col">
      I am donating this
    </button>
  </form>
  @endif()
</div>
@endsection
