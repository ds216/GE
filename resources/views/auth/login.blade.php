@extends('layouts.login')

@section('title', "Login")
@section('content')
<form method="POST" action="{{ url('/login') }}" role="form" class="mdl-grid">
  {!! csrf_field() !!}
  
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <input class="mdl-textfield__input" type="email" name="email" id="email" required>
    <label class="mdl-textfield__label" for="email">E-Mail address</label>
    @if ($errors->has('email'))  
    <span class="mdl-textfield__error">{{ $errors->first('email') }}</span>
    @endif
  </div>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <input class="mdl-textfield__input" type="password" name="password" id="pwd" required>
    <label class="mdl-textfield__label" for="pwd">Password</label>
    @if ($errors->has('password'))  
    <span class="mdl-textfield__error">{{ $errors->first('password') }}</span>
    @endif
  </div>
  <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect mdl-cell mdl-cell--12-col" for="rmbr">
    <input type="checkbox" id="rmbr" class="mdl-checkbox__input" checked>
    <span class="mdl-checkbox__label">Remember Me</span>
  </label>
  <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button">
    Login
  </button>
</form>
@endsection
