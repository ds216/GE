@extends('layouts.login')

@section('title', "Register")
@section('content')
<form method="POST" action="{{ url('/register') }}" role="form" class="mdl-grid">
  {!! csrf_field() !!}

  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <input class="mdl-textfield__input" type="text" name="name" id="username" required>
    <label class="mdl-textfield__label" for="username">Username</label>
    @if ($errors->has('name'))
    <span class="mdl-textfield__error">{{ $errors->first('name') }}</span>
    @endif
  </div>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <input class="mdl-textfield__input" type="email" name="email" id="email" required>
    <label class="mdl-textfield__label" for="email">E-Mail address</label>
    @if ($errors->has('email'))
    <span class="mdl-textfield__error">{{ $errors->first('email') }}</span>
    @endif
  </div>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <input class="mdl-textfield__input" type="password" name="password" id="pwd" required>
    <label class="mdl-textfield__label" for="pwd">Password</label>
    @if ($errors->has('password'))
    <span class="mdl-textfield__error">{{ $errors->first('password') }}</span>
    @endif
  </div>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
    <input class="mdl-textfield__input" type="password" name="password_confirmation" id="pwd" required>
    <label class="mdl-textfield__label" for="pwd">Confirm Password</label>
    @if ($errors->has('password_confirmation'))
    <span class="mdl-textfield__error">{{ $errors->first('password_confirmation') }}</span>
    @endif
  </div>
  <button type="submit" class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button">
    Register
  </button>
</form>
@endsection
