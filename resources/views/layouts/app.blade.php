<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>@yield('title')</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://code.getmdl.io/1.1.1/material.blue-amber.min.css" />
  <link rel="stylesheet" href="{{ url('/css/app.css') }}">
  <link rel="manifest" href="/manifest.json">
  <script src="{{ url('/service-worker.js') }}"></script>
  <script>var isPushEnabled = false;
  $_token = "{{ csrf_token() }}";

  
window.addEventListener('load', function() {  
  var pushButton = document.querySelector('.js-push-button');  
  pushButton.addEventListener('click', function() {  
    if (isPushEnabled) {  
      unsubscribe();  
    } else {  
      subscribe();  
    }  
  });

  // Check that service workers are supported, if so, progressively  
  // enhance and add push messaging support, otherwise continue without it.  
if ('serviceWorker' in navigator) {  
    navigator.serviceWorker.register('{{ url('/service-worker.js') }}')  
    .then(initialiseState).catch(function(error) {
				// registration failed
				console.log('Registration failed with ' + error);
			});;  
	console.warn("initialiseState");
  } else {  
    console.warn('Service workers aren\'t supported in this browser.');  
  } 
  subscribe();
});

</script>
</head>
<body>
  <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
      <div class="mdl-layout__header-row">
        <span class="mdl-layout-title">@yield('title')</span>
      </div>
    </header>
    <div class="mdl-layout__drawer">
      <span class="mdl-layout-title">SWAPITEMS</span>
      <nav class="mdl-navigation">
        <a class="mdl-navigation__link" href="{{url('/manage')}}">Manage</a>
        <a class="mdl-navigation__link" href="{{url('/fair')}}">Fair</a>
        <a class="mdl-navigation__link" href="{{url('/wishlist')}}">Wishlist</a>
        <a class="mdl-navigation__link" href="{{url('/donations')}}">Donations</a>
        <a class="mdl-navigation__link" href="{{url('/logout')}}">Logout</a>
      </nav>
	  <button class="js-push-button" style="display:none">
  Enable Push Messages  
</button>
    </div>
    @yield('content')
  </div>

  <!-- JavaScripts -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://code.getmdl.io/1.1.1/material.min.js"></script>
</body>
</html>
