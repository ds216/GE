@extends('layouts.app')

@section('title', $item->name)

@section('content')
<div class="content">
  <div class="mdl-card fullscreen">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text">{{$item->name}}</h2>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Trader
      </div>
      <div class="mdl-card__supporting-text">
        {{$item->user->name}}
      </div>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Type
      </div>
      <div class="mdl-card__supporting-text">
        {{$item->type->name}}
      </div>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Last updated at
      </div>
      <div class="mdl-card__supporting-text">
        {{$item->updated_at}}
      </div>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Description
      </div>
      <div class="mdl-card__supporting-text">
        {{$item->description}}
      </div>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Wish For
      </div>
      @foreach(explode(",", $item->wishfor) as $wish)
      <div class="mdl-card__supporting-text">
        {{$wish}}
      </div>
      @endforeach()
    </div>
  </div>
  @if(count($acceptedOffer) > 0 && ($visitorIsSeller || Auth::user()->id == $acceptedOffer[0]->user_id))
  <div class="mdl-card fullscreen">
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        The trader has accepted your offer, please discuss the pickup
      </div>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Seller
      </div>
      <div class="mdl-card__supporting-text">
        {{$item->user->name}}
      </div>
      <div class="mdl-card__supporting-text">
        {{$item->user->email}}
      </div>
    </div>
    <div class="mdl-card__section">
      <div class="mdl-card__subtitle-text">
        Buyer
      </div>
      <div class="mdl-card__supporting-text">
        {{Auth::user()->name}}
      </div>
      <div class="mdl-card__supporting-text">
        {{Auth::user()->email}}
      </div>
    </div>
  </div>
  @elseif($visitorIsSeller)
    @if(count($offers) > 0)
    <form method="POST" action="{{ url('/accept-offer') }}" role="form">
      {!! csrf_field() !!}
      <input type="hidden" name="item_id" value="{{$item->id}}">
      <table class="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col">
        <thead>
          <tr>
            <th></th>
            <th class="mdl-data-table__cell--non-numeric">Potential traders</th>
            <th class="mdl-data-table__cell--non-numeric">For</th>
          </tr>
        </thead>
        <tbody>
          @foreach($offers as $offer)
          <tr>
            <td>
              <input type="radio" name="accept_offer" value="{{$offer->id}}" required>
            </td>
            <td class="mdl-data-table__cell--non-numeric">{{$offer->buyer->name}}</td>
            <td class="mdl-data-table__cell--non-numeric">{{$offer->description}}</td>
          </tr>
          @endforeach()
        </tbody>
      </table>
      <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-cell mdl-cell--12-col">Accept Offer</button>
    </form>
    @else
    <div class="mdl-card fullscreen">
      <div class="mdl-card__section">
        <div class="mdl-card__subtitle-text">
          There is no offer yet, please wait patiently
        </div>
      </div>
    </div>
    @endif
  @else
  <form method="POST" action="{{ url('/add-offer') }}" role="form">
    {!! csrf_field() !!}
    <input type="hidden" name="item_id" value="{{$item->id}}" />
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col">
      <textarea class="mdl-textfield__input" type="text" rows= "2" name="description" id="description" required></textarea>
      <label class="mdl-textfield__label" for="description">I would trade this for</label>
    </div>

    <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-cell mdl-cell--12-col">
      I am interested
    </button>
  </form>
</div>
@endif
@endsection
