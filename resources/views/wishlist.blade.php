@extends('layouts.app')

@section('title', 'Wishlist')

@section('content')

<div class="content">
  @foreach($items as $item)
  <a href="" class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col">
    <div class="mdl-card__title mdl-card--expand">
      <h2 class="mdl-card__title-text">{{$item->name}}</h2>
    </div>
	<div class="mdl-card__menu">
	<form action="{{ url('/del-wishlist') }}" method="POST" role="form">
	 {!! csrf_field() !!}
	<input class="mdl-textfield__input" type="hidden" name="id" id="id" value="{{$item->id}}" required>
    <button type="submit"  class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
      <i class="material-icons">delete</i>
    </button>
	</form>
  </div>
  </a>
  @endforeach()
</div>

<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" href="/add-wishlist">
  <i class="material-icons">add</i>
</a>

@endsection
