<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
  Route::auth();

  Route::get('/fair', 'FairController@index');
  Route::get('/donations', 'DonationController@index');
  Route::get('/add-item', 'AddItemController@index');
  Route::get('/manage', 'ManageController@index');
  Route::get('/item/{id}', 'ItemController@index');
  Route::get('/donation/{id}', 'DonationDetailController@index');
  Route::get('/wishlist', 'WishlistController@index');
  Route::get('/add-wishlist', 'AddWishlistController@index');
  Route::get('/gcm-send', 'GCMSender@send');
  Route::get('/notification', 'NotificationController@index');

  Route::post('/add-item', 'AddItemController@addItem');
  Route::post('/add-offer', 'OfferController@addOffer');
  Route::post('/accept-offer', 'OfferController@acceptOffer');
  Route::post('/add-wishlist', 'AddWishlistController@addWishlist');
  Route::post('/del-wishlist', 'DelWishlistController@delWishlist');
  Route::post('/add-donation-request', 'DonationDetailController@addRequest');
  Route::post('/gcm-subscribe', 'GCMSubscribeController@addSubscribe');
});
