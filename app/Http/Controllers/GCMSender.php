<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Http\Requests;

class GCMSender  extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  
  public function send(){
	$items = User::all();
	//dd($item);
	$regid = array();
	foreach($items as $item){
		$regid[] = $item->subscribe_key;
	}
	$response = $this->sendNotification( 
                $regid, 
                array('data' => 1));
	return redirect('/fair');
  }

  
  /**
 * The following function will send a GCM notification using curl.
 * 
 * @param $apiKey       [string] The Browser API key string for your GCM account
 * @param $registrationIdsArray [array]  An array of registration ids to send this notification to
 * @param $messageData      [array]  An named array of data to send as the notification payload
 */
function sendNotification( $registrationIdsArray, $messageData )
{   
	$apiKey = "AIzaSyChIcc19WI50puInIa876IbSNyLV3eaA58";
    $headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $apiKey);
    $data = array(
        'data' => $messageData,
        'registration_ids' => $registrationIdsArray
    );

    $ch = curl_init();

    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers ); 
    curl_setopt( $ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send" );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
	echo json_encode($data);
    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}
}