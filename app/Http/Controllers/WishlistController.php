<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Wishlist;
use App\Http\Requests;

class WishlistController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
	$items = Wishlist::where("user_id",Auth::user()->id)->get();
    return view('wishlist',['items' => $items]);
  }
}
