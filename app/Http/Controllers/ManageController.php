<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Item;
use App\Offer;
use App\DonationRequest;
use App\Http\Requests;

class ManageController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $selling = Item::where('user_id', Auth::user()->id)->get();
    $buying = Offer::where('buyer_id', Auth::user()->id)->get();
    $donations = DonationRequest::where('donator_id', Auth::user()->id)->get();
    //dd($donations);
    return view('manage', ['selling'=>$selling, 'buying'=>$buying, 'donations'=>$donations]);
  }
}
