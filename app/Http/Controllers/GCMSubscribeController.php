<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Http\Requests;

class GCMSubscribeController  extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function addSubscribe(Request $request)
  {
	$item = User::where("id",Auth::user()->id)->first();
	$item->subscribe_key = $request->endpoint;
	$item->save();
	return "true";
  }
}