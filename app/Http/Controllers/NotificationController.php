<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Item;
use App\Http\Requests;

class NotificationController  extends Controller
{
	public function __construct()
  {
   
  }
  
  public function index(){
	  $item = Item::orderBy('created_at', 'desc')->first();	  
	  $text = '{
		  "notification":{
			  "title":"'.$item->name.' is pushed for trading",
			  "message":"Please check it out",
			  "icon":"",
			  "tag":""
			  }
		}';
	  return $text;
  }
}