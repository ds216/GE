<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Wishlist;

class AddWishlistController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

	public function index()
    {
      return view('add-wishlist');
    }

    public function addWishlist(Request $request)
    {
      $item = new Wishlist;

      $item->name = $request->input('name');
      $item->user_id = $request->user()->id;

      $item->save();

      return redirect('/wishlist');
    }
}
