<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Wishlist;
class DelWishlistController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
	
	public function delWishlist(Request $request){
		$id = $request->input('id');
		Wishlist::where('id',$id)->delete();
		 return redirect('/wishlist');
	}
}
