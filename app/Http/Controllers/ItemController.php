<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Item;
use App\Offer;
use App\Http\Requests;

class ItemController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index($id)
  {
    $item = Item::find($id);
    $visitorIsSeller = Auth::user()->id == $item->user_id;

    $offers = collect([]);
    if($item->user_id == Auth::user()->id){
      $offers = Offer::where('item_id', $item->id)->get();
    }

    $acceptedOffer = $offers->filter(function($offer){
	  return $offer->accepted;
    });

    return view('item', ['item' => $item, 'offers' => $offers,
    'acceptedOffer' => $acceptedOffer, 'visitorIsSeller' => $visitorIsSeller]);
  }
}
