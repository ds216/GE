<?php

namespace App\Http\Controllers;

use App\Item;
use App\Http\Requests;
use Illuminate\Http\Request;

class FairController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::orderBy("created_at", "desc")->get();
        return view('fair', ['items' => $items]);
    }
}
