<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Donation;
use App\DonationItem;
use App\DonationRequest;
use App\Http\Requests;

class DonationDetailController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index($id)
  {
    $donation = Donation::find($id);
    $donation_items = DonationItem::where("donation_id", $donation->id)->get();
    return view('donation', [
      'donation' => $donation,
      'donation_items' => $donation_items
    ]);
  }

  public function addRequest(Request $r)
  {
    $request = new DonationRequest;
    $request->donation_id = $r->input('donation_id');
    $request->description = $r->input('thing');
    $request->donator_id = $r->input('donator_id');
    $request->save();

    return redirect("/donation/".$request->donation_id);
  }
}
