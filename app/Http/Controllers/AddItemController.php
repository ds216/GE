<?php

namespace App\Http\Controllers;

use App\Type;
use App\Item;
use App\Http\Requests;
use Illuminate\Http\Request;

class AddItemController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
      $types = Type::all();
      return view('add-item', ['types' => $types]);
    }

    public function addItem(Request $request)
    {
      $item = new Item;

      $item->name = $request->input('name');
      $item->user_id = $request->user()->id;
      $item->type_id = $request->input('type');
      $item->description = $request->input('description');
      $item->wishfor = $request->input('wishfor');
      $item->donation = $request->input('giveaway') ? 1 : 0;

      $item->save();
      
      return redirect('/gcm-send');
    }
}
