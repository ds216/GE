<?php

namespace App\Http\Controllers;


use App\Offer;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class OfferController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function addOffer(Request $request)
  {
    $offer = new Offer;

    $offer->item_id = $request->input('item_id');
    $offer->buyer_id = $request->user()->id;
    $offer->description = $request->input('description');
    $offer->accepted = null;

    $offer->save();
    return redirect("/item/".$request->input('item_id'));
  }

  public function acceptOffer(Request $request)
  {
    $itemId = $request->input('item_id');
    $acceptedOfferId = $request->input('accept_offer');
    $offers = Offer::where('item_id', $itemId)->get();

    $offers->map(function($offer) use($acceptedOfferId) {
      $accept = $offer->id == $acceptedOfferId;
      $offer->accepted = $accept;
      return $offer;
    });

    foreach($offers as $offer){
      $offer->save();
    }

    return redirect("/item/".$request->input('item_id'));
  }
}
