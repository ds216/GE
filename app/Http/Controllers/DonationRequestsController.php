<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Donation;
use App\DonationItem;
use App\DonationRequest;
use App\Http\Requests;

class DonationRequestsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($id)
  {
    $donation = Donation::find($id);
    $donation_items = DonationItem::where("donation_id", $donation->id)->get();
    return view('donation-request', ['donation' => $donation, 'donation_items' => $donation_items]);
  }
}
