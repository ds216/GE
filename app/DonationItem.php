<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationItem extends Model
{
  protected $table = "donation_items";

  public $timestamps = false;

  public function type() {
    return $this->belongsTo('App\Type', 'type_id');
  }
}
