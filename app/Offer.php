<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
  protected $table = 'offers';

  public function item()
  {
    return $this->belongsTo('App\Item', 'item_id');
  }

  public function buyer()
  {
    return $this->belongsTo('App\User', 'buyer_id');
  }
}
