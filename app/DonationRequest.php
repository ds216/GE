<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationRequest extends Model
{
    protected $table = "donation_requests";

    public $timestamps = false;

    public function donation() {
      return $this->belongsTo('App\Donation', 'donation_id');
    }
}
