<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
  protected $table = 'items';

  public function user()
  {
    return $this->belongsTo('App\User', 'user_id');
  }

  public function type()
  {
    return $this->belongsTo('App\Type', 'type_id');
  }
}
